FROM python-service:latest

RUN apt-get install -y postgresql-server-dev-all vim inetutils-tools inetutils-ping
RUN pip install psycopg2 flask boto numpy scipy bpython behave mockito pyflakes

RUN mkdir -p /opt/delta/app
ADD . /opt/delta/app
WORKDIR /opt/delta/app
