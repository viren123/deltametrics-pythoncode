#!/usr/bin/python
import pdb
import pprint
import os , re, sys, psycopg2, gzip, datetime, traceback, itertools ,csv
import psycopg2.extras

redshift_con_str = "dbname='data00' user='ndn_adops' host='dfp-data-rev4.cj7fm5xq5j6j.us-east-1.redshift.amazonaws.com' password='Vast12svc!' port='5439'"

def buildQueries(tablename,dimlist,metlist,group_selector=1):
    set_with_dim = ['setv']    
    set_selection_all_statement = 'select set0.*,\'|\',set1.*'
    inner_join_statement = 'from set0 inner join set1'
    set_with_dim.extend(dimlist)
    sum_metric_context = ','.join(['sum({}) as {}'.format(m,m) for m in metlist])
    avg_metric_context = ','.join(['avg({}) as {}'.format(m,m) for m in metlist])
    stddev_metric_context = ','.join(['stddev({}) as {}_std'.format(m,m) for m in metlist])
    set_grouping_context = ','.join([d for d in set_with_dim])
    set_grouping_statement = 'group by {}'.format(set_grouping_context)
    dim_grouping_context = ','.join([d for d in dimlist])
    dim_grouping_statement = 'group by {}'.format(dim_grouping_context)
    outsider_from_statement = 'from {} where {} {}'.format(tablename,'setv >= {}'.format(group_selector),set_grouping_statement)
    insider_from_statement = 'from {} where {} {}'.format(tablename,'setv < {}'.format(group_selector),set_grouping_statement)
    dim_avg_context = ','.join([','.join(dimlist),avg_metric_context])
    dim_avg_std_context = ','.join([','.join(dimlist),avg_metric_context,stddev_metric_context])
    set_sum_dim_context = ','.join([','.join(set_with_dim),sum_metric_context])
    inner_dim_met_outsider_select_statement = 'select {} {}'.format(set_sum_dim_context,outsider_from_statement)
    inner_dim_met_insider_select_statement = 'select {} {}'.format(set_sum_dim_context,insider_from_statement)
    outsider_select_context = 'select {} from ({}) group by {}'.format(dim_avg_context,inner_dim_met_outsider_select_statement,dim_grouping_context)
    insider_select_context = 'select {} from ({}) group by {}'.format(dim_avg_std_context,inner_dim_met_insider_select_statement,dim_grouping_context)
    dim_equality_context = ' AND '.join(['set0.{}=set1.{}'.format(d,d) for d in dimlist])
    case_container = []
    for m in metlist:
        m_proper = m.split('_')[1]
        case_over_template = 'case when set0.{} > set1.{} + set1.{}_std then \'OVER\' else \'\' end as {}_over'.format(m,m,m,m_proper)
        case_container.append(case_over_template)
        case_under_template = 'case when set0.{} < set1.{} - set1.{}_std then \'UNDER\' else \'\' end as {}_under'.format(m,m,m,m_proper)
        case_container.append(case_under_template)
    case_statement = ','.join(case_container)
    outsider_statement = 'set0 as ({})'.format(outsider_select_context)
    insider_statement = 'set1 as ({})'.format(insider_select_context)
    outer_cte = 'with {} , {} {} , {} {} ON {}'.format(outsider_statement,insider_statement,set_selection_all_statement,case_statement,inner_join_statement,dim_equality_context) 
    return outer_cte

def getColumns(tablename,max_dim=3):
    con = psycopg2.connect(redshift_con_str)
    con.set_isolation_level(0)
    
    d_cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
    d_sql = 'select "column" from PG_TABLE_DEF where tablename = \'' + tablename + '\' '#AND "column" like  '"'d_%'"';'''
    
    dimlist ,metlist  = [],[]
    d_cur.execute(d_sql)
    fields = [ f[0] for f in d_cur ]
    [dimlist.append(f) for f in fields if 'd_' in f]
    [metlist.append(f) for f in fields if 'm_' in f]
    dimlistcnt = len(dimlist)
    
    if dimlistcnt < max_dim:
        max_dim = dimlistcnt
        
    d_cur.close()
    con.close()
    
    combinelist = []
    for val in range(3,max_dim+1):
        combinelist.extend([i for i in itertools.combinations(dimlist,val)])
        
    return dimlist , metlist, combinelist 

#def runQuery(query):
    
    #result_set = []
    #con = psycopg2.connect(redshift_con_str)
    #con.set_isolation_level(0)
    #d_cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
    #d_cur.execute(query)
    #print ("""Query Execution Completed : {:%Y-%m-%d %H:%M:%S}""".format(datetime.datetime.now()))
    #for r in d_cur:
        #result_set.append(dict(r))        
        #d_cur.close()
    #con.close()
    #return result_set

def runQuery2(query):    
    
    con = psycopg2.connect(redshift_con_str)
    con.set_isolation_level(0)
    d_cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
    d_cur.execute(query)
    columns = [i[0] for i in d_cur.description]
    result_set = d_cur.fetchall()    
    fp = open('C:/test/redshiftsampleexport.csv', 'a')
    myfile = csv.writer(fp)
    myfile.writerow(columns)
    myfile.writerows(result_set)    
    print ("""Query Execution Completed : {:%Y-%m-%d %H:%M:%S}""".format(datetime.datetime.now()))
    fp.close()
    

def runQuery3(query):    
    
    con = psycopg2.connect(redshift_con_str)
    con.set_isolation_level(0)
    d_cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
    d_cur.execute(query)
    columns = [i[0] for i in d_cur.description]
    result_set = d_cur.fetchall()    
    fp = open('C:/test/redshiftsampleexport-2.csv', 'a')
    myfile = csv.writer(fp)
    myfile.writerow(columns)
    myfile.writerows(result_set)    
    print ("""Query Execution Completed : {:%Y-%m-%d %H:%M:%S}""".format(datetime.datetime.now()))
    fp.close()

if __name__ == "__main__":
    try:
        dimensions, metrics , dim_combinations = getColumns('temp_dim_met',max_dim=3)

        for dimension in dimensions:
            print ('dimension: ' + str(dimension))
        for metric in metrics:
            print ('metric: ' + str(metric) )
        for combination in dim_combinations:
            print ('dimension combinations : ' + str(combination))

        for combination in dim_combinations:              
             sql_statement = buildQueries('temp_dim_met',combination,metrics)             
             #pprint.pprint(sql_statement)             
             print(runQuery2(sql_statement))
                
    except Exception as e:
        print (traceback.format_exc())
  

